import React from 'react'
import { render } from 'react-dom'
import {NotFound} from './components/not-found'
import { SkiDayCount } from './components/SkiDayCount'
import { Menu } from './Menu';
import { Add } from './components/Add';
import { Router, Route, hashHistory, browserHistory} from 'react-router'


window.React = React

render(

    <Router history={hashHistory}>
        <Route path="/" component={SkiDayCount} />
        <Route path="/add" component={Add} />
        <Route path="*" component={NotFound} />
    </Router>,
    document.getElementById("react-container")
)