import {Link} from 'react-router'

export const Menu = () =>
    <nav className="menu">
        <Link to="/" activeClassName="selected" ></Link>
        <Link to="/not-found"  activeClassName="selected">Not Found</Link>
    </nav>
