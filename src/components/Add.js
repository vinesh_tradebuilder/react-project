import { Component } from 'react'


export class Add extends Component {


    render() {
        const {name, email} = this.props;
        return (
            <form>
                <div>
                    <label htmlFor="name">Name</label>
                    <input id="name" required defaultValue={name}/>
                </div>

                <div>
                    <label htmlFor="email">Email</label>
                    <input id="email" required defaultValue={email} />
                </div>

                <div>
                    <input type="submit" value="Add" />
                </div>

            </form>
        )
    }
}

Add.defaultProps = {
    name: 'vinesh',
    email: 'vcinesh@gmail.com'
}